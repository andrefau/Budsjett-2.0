import { BudsjettUIPage } from './app.po';

describe('budsjett-ui App', function() {
  let page: BudsjettUIPage;

  beforeEach(() => {
    page = new BudsjettUIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
